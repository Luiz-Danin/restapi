-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 02-Fev-2021 às 13:44
-- Versão do servidor: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `apinofaro`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atendimentos`
--

CREATE TABLE `atendimentos` (
  `id` int(11) NOT NULL,
  `pet_id` int(11) NOT NULL,
  `descricao` text DEFAULT NULL,
  `data_atendimento` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `atendimentos`
--

INSERT INTO `atendimentos` (`id`, `pet_id`, `descricao`, `data_atendimento`, `updated_at`, `created_at`) VALUES
(1, 1, 'Dor de barriga', '2021-02-02 02:45:22', '2021-02-02 12:44:29', '0000-00-00 00:00:00'),
(2, 44, NULL, '0000-00-00 00:00:00', '2021-02-02 12:52:32', '2021-02-02 12:52:32'),
(3, 45, NULL, '0000-00-00 00:00:00', '2021-02-02 12:54:25', '2021-02-02 12:54:25'),
(4, 49, NULL, NULL, '2021-02-02 12:57:27', '2021-02-02 12:57:27'),
(5, 51, NULL, NULL, '2021-02-02 12:59:09', '2021-02-02 12:59:09'),
(7, 58, 'texto da lala', NULL, '2021-02-02 13:07:08', '2021-02-02 13:07:08'),
(8, 59, 'texto da lala', NULL, '2021-02-02 13:07:26', '2021-02-02 13:07:26'),
(9, 60, 'texto da lala', '2021-02-02 00:00:00', '2021-02-02 13:08:35', '2021-02-02 13:08:35'),
(10, 61, 'texto da lala', '2021-02-02 13:09:02', '2021-02-02 13:09:02', '2021-02-02 13:09:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pets`
--

CREATE TABLE `pets` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `especie` varchar(50) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pets`
--

INSERT INTO `pets` (`id`, `nome`, `especie`, `updated_at`, `created_at`) VALUES
(1, 'Lala Flor', 'Cão', '2021-02-01 12:13:41', '2021-02-01 04:46:37'),
(3, 'Perola', 'Gato', '2021-02-01 12:04:56', '2021-02-01 12:04:56'),
(4, 'Duque', 'cão', '2021-02-01 12:39:10', '2021-02-01 12:39:10'),
(5, 'Spotify', 'Gato', '2021-02-01 12:39:26', '2021-02-01 12:39:26'),
(6, 'Lion', 'Gato', '2021-02-01 12:39:33', '2021-02-01 12:39:33'),
(7, 'Leite', 'Gato', '2021-02-01 12:39:41', '2021-02-01 12:39:41'),
(8, 'Nelsinho', 'Gato', '2021-02-01 12:40:29', '2021-02-01 12:39:57'),
(9, 'Tonilda', 'Gato', '2021-02-01 12:40:08', '2021-02-01 12:40:08'),
(10, 'flor', 'c', '2021-02-02 04:58:33', '2021-02-02 04:58:33'),
(11, 'flor', 'c', '2021-02-02 05:02:20', '2021-02-02 05:02:20'),
(12, 'flo_petr', 'c', '2021-02-02 12:24:18', '2021-02-02 12:24:18'),
(13, 'flo_petr', 'c', '2021-02-02 12:24:48', '2021-02-02 12:24:48'),
(14, 'flo_petr', 'c', '2021-02-02 12:25:15', '2021-02-02 12:25:15'),
(15, 'flo_petr', 'c', '2021-02-02 12:25:32', '2021-02-02 12:25:32'),
(16, 'flo_petr', 'c', '2021-02-02 12:26:23', '2021-02-02 12:26:23'),
(17, 'flo_petr', 'c', '2021-02-02 12:26:43', '2021-02-02 12:26:43'),
(18, 'flo_petr', 'c', '2021-02-02 12:27:12', '2021-02-02 12:27:12'),
(19, 'flo_petr', 'c', '2021-02-02 12:27:26', '2021-02-02 12:27:26'),
(20, 'flo_petr', 'c', '2021-02-02 12:27:42', '2021-02-02 12:27:42'),
(21, 'flo_petr', 'c', '2021-02-02 12:27:52', '2021-02-02 12:27:52'),
(22, 'flo_petr', 'c', '2021-02-02 12:30:25', '2021-02-02 12:30:25'),
(23, 'flo_petr', 'c', '2021-02-02 12:30:40', '2021-02-02 12:30:40'),
(24, 'flo_petr', 'c', '2021-02-02 12:30:51', '2021-02-02 12:30:51'),
(25, 'flo_petr', 'c', '2021-02-02 12:32:22', '2021-02-02 12:32:22'),
(26, 'flo_petr', 'c', '2021-02-02 12:33:38', '2021-02-02 12:33:38'),
(27, 'flo_petr', 'c', '2021-02-02 12:35:29', '2021-02-02 12:35:29'),
(28, 'flo_petr', 'c', '2021-02-02 12:36:02', '2021-02-02 12:36:02'),
(29, 'flo_petr', 'c', '2021-02-02 12:36:47', '2021-02-02 12:36:47'),
(30, 'flo_petr', 'c', '2021-02-02 12:37:51', '2021-02-02 12:37:51'),
(31, 'flo_petr', 'c', '2021-02-02 12:38:28', '2021-02-02 12:38:28'),
(32, 'flo_petr', 'c', '2021-02-02 12:39:00', '2021-02-02 12:39:00'),
(33, 'flo_petr', 'c', '2021-02-02 12:39:14', '2021-02-02 12:39:14'),
(34, 'flo_petr', 'c', '2021-02-02 12:41:27', '2021-02-02 12:41:27'),
(35, 'flo_petr', 'c', '2021-02-02 12:41:59', '2021-02-02 12:41:59'),
(36, 'flo_petr', 'c', '2021-02-02 12:42:39', '2021-02-02 12:42:39'),
(37, 'flo_petr', 'c', '2021-02-02 12:43:30', '2021-02-02 12:43:30'),
(38, 'flo_petr', 'c', '2021-02-02 12:44:33', '2021-02-02 12:44:33'),
(39, 'flo_petr', 'c', '2021-02-02 12:46:58', '2021-02-02 12:46:58'),
(40, 'flo_petr', 'c', '2021-02-02 12:48:42', '2021-02-02 12:48:42'),
(41, 'flo_pet', 'c', '2021-02-02 12:49:18', '2021-02-02 12:49:18'),
(42, 'flo_pet', 'c', '2021-02-02 12:50:03', '2021-02-02 12:50:03'),
(43, 'flo_pet', 'c', '2021-02-02 12:51:00', '2021-02-02 12:51:00'),
(44, 'flo_pet', 'c', '2021-02-02 12:52:31', '2021-02-02 12:52:31'),
(45, 'flo_pet', 'c', '2021-02-02 12:54:25', '2021-02-02 12:54:25'),
(46, 'flo_pet', 'c', '2021-02-02 12:55:02', '2021-02-02 12:55:02'),
(47, 'flo_pet', 'c', '2021-02-02 12:56:51', '2021-02-02 12:56:51'),
(48, 'flo_pet', 'c', '2021-02-02 12:57:01', '2021-02-02 12:57:01'),
(49, 'flo_pet', 'c', '2021-02-02 12:57:27', '2021-02-02 12:57:27'),
(50, 'flo_pet', 'c', '2021-02-02 12:58:43', '2021-02-02 12:58:43'),
(51, 'flo_pet', 'c', '2021-02-02 12:59:09', '2021-02-02 12:59:09'),
(52, 'flo_pet', 'c', '2021-02-02 13:02:13', '2021-02-02 13:02:13'),
(53, 'flo_pet', 'c', '2021-02-02 13:02:57', '2021-02-02 13:02:57'),
(54, 'flo_pet', 'c', '2021-02-02 13:04:11', '2021-02-02 13:04:11'),
(55, 'flo_pet', 'c', '2021-02-02 13:04:34', '2021-02-02 13:04:34'),
(56, 'flo_pet', 'c', '2021-02-02 13:06:01', '2021-02-02 13:06:01'),
(57, 'flo_pet', 'c', '2021-02-02 13:06:32', '2021-02-02 13:06:32'),
(58, 'flo_pet', 'c', '2021-02-02 13:07:07', '2021-02-02 13:07:07'),
(59, 'flo_pet', 'c', '2021-02-02 13:07:25', '2021-02-02 13:07:25'),
(60, 'flo_pet', 'c', '2021-02-02 13:08:35', '2021-02-02 13:08:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jhon', 'jhon@gmail.com', NULL, '$2y$10$fpOWcaSP/JOSrZcHVep7s.RqTXJgoLztZE4.M2HOEjqFr7FzNtTVi', 'VMnyR4CrT2W67Xr7fENl8NMY6immoEvbd6vRveoIB7gwjqvkhYxRbPVMqZ97mhzYhq86hVlSXvVJdGic', NULL, '2021-01-31 21:58:30', '2021-01-31 21:58:30');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `atendimentos`
--
ALTER TABLE `atendimentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pets` (`pet_id`);

--
-- Índices para tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices para tabela `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `atendimentos`
--
ALTER TABLE `atendimentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
