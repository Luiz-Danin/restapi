<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pet extends Model
{
    protected $fillable = ['nome','especie'];
    
    public function atendimento()
    {
        return $this->hasMany(atendimento::class);
    }
}
