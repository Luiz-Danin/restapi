<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class atendimento extends Model
{
    protected $fillable = ['pet_id','descricao','data_atendimento'];
    
    
    public function pet()
    {
        return $this->belongsTo(pet::class);
    }
}
