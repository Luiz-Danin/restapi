<?php
namespace App\Http\Controllers\API;

use App\pet;
use App\atendimento;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\PetResource;
use App\Http\Resources\AtendimentoResource;

class PetsController extends Controller
{
    /*
     * @var Pet
     */ 
    private $pet;

    public function __construct(Pet $pet)
    {
        $this->pet = $pet;
    }
    
    public function index()
    {
        $pet = $this->pet->paginate(5);
        
        return response()->json($pet);
    }
    
    public function show($nome)
    {
        $pet = $this->pet->where('nome','like','%'.$nome.'%')->first();
        //$atendimento = $this->pet->atendimento();
        $atendimento = $this->pet->find($pet->id)->atendimento;

        //$pet_resource =  PetResource($pet);
        return response()->json(['pet'=>$pet,'atendimento'=>$atendimento]);
    }
    
    public function save(Request $request)
    {
        
        $data = $request->all();
        $pet = $this->pet->create($data);
        if( isset($pet->id) )
        {
            $atendimento = new atendimento();
            $atendimento->pet_id = $pet->id;
            $atendimento->descricao = $data['descricao'];
            $atendimento->data_atendimento = date("Y-m-d H:i:s");
            $atendimento->save();
        }
        
        return response()->json( $pet );
    }
    
    public function update(Request $request)
    {
        $data = $request->all();
        
        $pet = $this->pet->find($data['id']);
        $pet->update($data);
        
        if( isset($pet) )
        {

        }
        
        return response()->json( $pet );
    }
    
    public function delete($id)
    {
         $pet = $this->pet->find($id);
         
         if (isset($id))
         {
             
            $atendimento = new atendimento();
            $atendimento->id = $id;
            $atendimento->delete();
            
            $pet->delete();
         }
          
         return response()->json(['data'=>['mensagem'=>'O Pet foi removido com sucesso']]);
    }
}
