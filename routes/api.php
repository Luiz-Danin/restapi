<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('profile','API\UserController@getProfile');
//Route::get('user-list','API\UserController@getUserList');
    
Route::get('test', function( Request $request){
    //dd($request->headers->all());
    //dd($request->headers->get('user-agent'));
    
    $response = new \Illuminate\Http\Response(json_encode(['msg'=>'Minha Primeira resposta de API']) );
    $response->header('Content-Type', 'application/json');
    
    return $response;
});

Route::namespace('API')->prefix('pets')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace
    Route::get('/', 'PetsController@index');
    Route::get('/{nome}', 'PetsController@show');
    Route::post('/','PetsController@save');
    Route::put('/','PetsController@update');
    Route::delete('/{id}','PetsController@delete');
});        


//Route::get('login','API\UserController@getUserlogin');